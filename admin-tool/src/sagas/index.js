import { fork } from 'redux-saga/effects';
import { imageSaga } from './image';

export default function* createMainSaga() {
    yield fork(imageSaga);
}
