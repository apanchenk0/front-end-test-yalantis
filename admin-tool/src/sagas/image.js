import { put, call, all, takeEvery } from 'redux-saga/effects';
import {
    CLOSE_ADD_IMAGE_MODAL,
    DELETE_IMAGE,
    HIDE_IMAGE_DETAILS, IS_IMAGES_LOADING,
    LOAD_ALL_IMAGES, SAVE_EDITION_IMAGE,
    SET_IMAGE_DATA,
    UPLOAD_IMAGE
} from '../types/image';
import ImageService from '../services/image-service';

const API = new ImageService();

function* loadAllImages() {
    try {
        yield put({type: IS_IMAGES_LOADING, payload: true});
        const response = yield call(API.loadAll);
        yield put({type: SET_IMAGE_DATA, payload: response });
        yield put({type: IS_IMAGES_LOADING, payload: false});
    } catch(error) {
        console.log(error);
    }
}

function* uploadImage({ payload: image }) {
    try {
        // Загружаем новое изображение
        yield call(API.uploadOne, image);
        // Получаем новые данные
        yield put({type: LOAD_ALL_IMAGES});
        // Закрываем модальное окно
        yield put({type: CLOSE_ADD_IMAGE_MODAL});
    } catch(error) {
        console.log(error)
    }
}

function* deleteImage({ payload: id }) {
    try {
        // Удаляем изображение
        yield call(API.deleteOne, id);
        // Получаем новые данные
        yield put({type: LOAD_ALL_IMAGES});
        // Закрываем подробности об изображение
        yield put({type: HIDE_IMAGE_DETAILS});
    } catch(error) {
        console.log(error)
    }
}

function* saveImage({ payload: image }) {
    try {
        // Обновляем изображение
        yield call(API.updateOne, image);
        // Получаем новые данные
        yield put({type: LOAD_ALL_IMAGES});
    } catch(error) {
        console.log(error)
    }
}

export function* imageSaga() {
    yield all([takeEvery(LOAD_ALL_IMAGES, loadAllImages)]);
    yield all([takeEvery(UPLOAD_IMAGE, uploadImage)]);
    yield all([takeEvery(DELETE_IMAGE, deleteImage)]);
    yield all([takeEvery(SAVE_EDITION_IMAGE, saveImage)]);
}
