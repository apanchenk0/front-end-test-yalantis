import React from 'react';
import {StyledBtn} from './style';
import {connect} from 'react-redux';
import {openModalToAddImage} from '../../actions/image';

const AddNewImg = ({ openModalToAddImage }) => {

    const handleOpenModalWindow = (e) => {
        e.preventDefault();
        openModalToAddImage();
    }

    return <StyledBtn href={'#'} onClick={handleOpenModalWindow}>Add image</StyledBtn>
}

const mapDispatchToProps = {
    openModalToAddImage,
}

export default connect(null, mapDispatchToProps)(AddNewImg);
