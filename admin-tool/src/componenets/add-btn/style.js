import style from 'styled-components';

export const StyledBtn = style.a`
    z-index: 10;
    position: fixed;
    right: -20px;
    bottom: 100px;
    font-size: 16px;
    display: flex;
    justify-content: center;
    alight-items: center;
    text-transform: uppercase;
    font-weight: bold;
    color: #fff;
    background-color: coral;
    text-decoration: none;
    padding: 10px 25px 10px 5px;
    border-radius: 5px 0 0 5px;
    transition: 0.3s;
    &:hover {
        right: 0px;
    }
`;
