import React, {Component} from 'react';
import {ImageWrap, StyledImg} from './style';
import {showImageDetails} from '../../actions/image';
import {connect} from 'react-redux';
import Tooltip from '../tooltip';

class Image extends Component {

    state = {
        isTooltipShow: false,
    }

    onHandleClick = (e) => {
        e.preventDefault();
        return this.props.onClick ? this.props.onClick(this.props.id) : null;
    }

    render() {
        const { data: {id, img, tooltip}, editable, onEdit, tooltipAlways, noAnimate } = this.props;
        return (
            <ImageWrap onClick={this.onHandleClick} noAnimate={noAnimate}>
                <div onMouseEnter={() => this.setState({isTooltipShow: true})} onMouseLeave={() => this.setState({isTooltipShow: false})}>
                    <StyledImg src={img} alt={id}/>
                    {(this.state.isTooltipShow || tooltipAlways) && tooltip ? <Tooltip tooltip={tooltip} onEdit={onEdit} editable={editable}/> : null}
                </div>
            </ImageWrap>
        );
    }
}

const mapDispatchToProps = {
    showImageDetails,
}

export default connect(null, mapDispatchToProps)(Image);
