import style from 'styled-components';

export const ImageWrap = style.div`
    padding: 10px;
    & > div {
        width: 260px;
        height: 260px;
        overflow: hidden;
        transform: scale(1.0);
        transition: 0.3s;
        position: relative;
        &:hover {
            cursor: pointer;
            transform: ${props => props.noAnimate ? 'scale(1.0)' : 'scale(1.1)'};
        }
    }
`;

export const StyledImg = style.img`
    width: 100%;
    height: 100%;
    object-fit: cover;
`;
