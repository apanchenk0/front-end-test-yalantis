import style, {keyframes} from 'styled-components';

const rotate = keyframes`
    from {
        transform: rotate(0deg)
    }
    to {
        transform: rotate(360deg)
    }
`;

export const LoaderWrap = style.div`
    width: 100%;
    height: 100vh;
    overflow: hidden;
    position: fixed;
    top: 0;
    left: 0;
    display: flex;
    flex-flow: column;
    justify-content: center;
    align-items: center;
`;

export const Loader = style.div`
    width: 100px;
    height: 100px;
    border-radius: 50%;
    position: relative;
    background-image: linear-gradient(90deg, transparent 50%, steelblue 50%);
    animation: ${rotate} 1s infinite;
    &::after {
        position: absolute;
        content: '';
        display: block;
        background: #fff;
        width: 90px;
        height: 90px;
        border-radius: 50%;
        top: 5px;
        left: 5px;
    }
`;
