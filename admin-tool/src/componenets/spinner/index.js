import React from 'react';
import {Loader, LoaderWrap} from './style';

const Spinner = () => {
    return (
        <LoaderWrap>
            <Loader />
            <h2>Loading ...</h2>
        </LoaderWrap>
    )
}

export default Spinner;
