import React, { Component } from 'react';
import { connect } from 'react-redux';
import {loadAllImages, showImageDetails} from '../../actions/image';
import Spinner from '../spinner';
import Image from '../image';
import {Grid} from './style';
import AddNewImg from '../add-btn';
import AddImgModal from '../add-img-modal';
import ShowDetailModal from '../show-image-details-modal';

class App extends Component {

    componentDidMount() {
        this.props.loadAllImages();
    }

    onHandleShowDetails = (id) => {
        this.props.showImageDetails(id);
    }

    render() {
        const { images, isModalOpen, showedImage, isImagesLoading } = this.props;
        return (
        <div>
            <AddNewImg />
            {isModalOpen && <AddImgModal />}
            {showedImage && <ShowDetailModal />}
            <Grid>
                {isImagesLoading && <Spinner />}
                {images.length === 0 && !isImagesLoading ? <h1>No images :(</h1> : images.map(img => <Image key={img.id} id={img.id} data={img} onClick={this.onHandleShowDetails}/>)}
            </Grid>
        </div>
        )
    }
}

const mapStateToProps = ({ imageReducer: { images, isModalOpen, showedImage, isImagesLoading } }) => ({images, isModalOpen, showedImage, isImagesLoading});

const mapDispatchToProps = { loadAllImages, showImageDetails };

export default connect(mapStateToProps, mapDispatchToProps)(App);
