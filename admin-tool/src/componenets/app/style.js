import style from 'styled-components';

export const Grid = style.div`
    display: flex;
    justify-content: center;
    align-items: flex-start;
    flex-flow: row;
    flex-wrap: wrap;
`;
