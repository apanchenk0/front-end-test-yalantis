import style from 'styled-components';

export const TooltipSettingWrap = style.div`
    background-color: #797979;
    border-radius: 10px;
    padding: 10px;
    position: relative;
    & > h2 {
        color: #fff;
        font-size: 14px;
        font-weight: bold;
        text-align: center;
        margin: 0 0 10px 0;
    }
    & > div {
        display: flex;
        justify-content: space-between;
        align-items: flex-start;
        & > div {
            display: flex;
            justify-content: flex-start;
            align-items: center;
            flex-flow: column;
            & > h3 {
                color: #fff;
                font-size: 12px;
                font-weight: bold;
                margin: 0px;
            }
        }
    }
`;

export const TooltipControls = style.div`
    display: flex;
    justify-content: flex-start!important;
    align-items: flex-end!important;
    position: absolute;
    top: 10px;
    right: 10px;
    & > a {
        text-decoration: none;
        display: flex;
        justify-content: flex-start;
        align-items: flex-end;
        width: 15px;
        height: 15px;
        margin: 0 3px;
    }
`;

export const ConfirmEditing = style.a`
    color: #66bb67;
`;

export const CloseEditing = style.a`
    color: #ff00009e;
`;

export const SettingsList = style.ul`
    list-style: none;
    display: flex;
    justify-content: flex-start;
    align-items: flex-start;
    flex-wrap: wrap;
    width: 90px;
    height: auto;
    padding: 0;
    margin: 10px 0 0 0;
`;

export const SettingsItem = style.li`
    display: flex;
    justify-content: center;
    align-items: center;
    flex: 0 0 33.3%;
    max-width: 33.3%;
    height: 30px;
`;

export const SettingsColorItem = style(SettingsItem)`
    &:nth-child(1) {
        & > a{
            background-color: lime;
        }
    }
    &:nth-child(2) {
        & > a{
            background-color: tomato;
        }
    }
    &:nth-child(3) {
        & > a{
            background-color: red;
        }
    }
    &:nth-child(4) {
        & > a{
            background-color: orange;
        }
    }
    &:nth-child(5) {
        & > a{
            background-color: blue;
        }
    }
    &:nth-child(6) {
        & > a{
            background-color: purple;
        }
    }
    &:nth-child(7) {
        & > a{
            background-color: yellow;
        }
    }
    &:nth-child(8) {
        & > a{
            background-color: aqua;
        }
    }
    &:nth-child(9) {
        & > a{
            background-color: pink;
        }
    }
`;

export const SettingsControl = style.a`
    padding: 3px;
    display: flex;
    justify-content: center;
    align-items: center;
    max-width: 100%;
    max-height: 100%;
    text-decoration: none;
    color: #000;
    &:hover {
        cursor: pointer;
        transform: scale(1.05);
        color: lime;
    }
`;

export const SettingsColorControl = style(SettingsControl)`
    padding: 10px;
    border: 1px solid #000;
    &:hover{
        border: 1px solid #fff;
    }
`;

export const DeleteTooltipButton = style.a`
    padding: 15px;
    color: #fff;
    text-transform: uppercase;
    font-size: 14px; 
    background-color: #ff00009e;
    border-radius: 5px;
    margin-top: 20px;
    transition: 0.3s;
    text-decoration: none;
    display: flex;
    justify-content: center;
    align-items: center;
    &:hover {
        transform: scale(1.1);
    }
`;
