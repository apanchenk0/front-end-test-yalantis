import React, {Component} from 'react';
import {
    CloseEditing,
    ConfirmEditing,
    DeleteTooltipButton,
    SettingsColorControl,
    SettingsColorItem,
    SettingsControl,
    SettingsItem,
    SettingsList, TooltipControls,
    TooltipSettingWrap
} from './style';

class   TooltipSettings extends Component {

    onHandleUpdateTooltip = (e, data) => {
        e.preventDefault();
        if(this.props.onUpdate) {
            this.props.onUpdate(data);
        }
    }

    onHandleDelete = (e) => {
        e.preventDefault();
        if(this.props.onDelete) {
            this.props.onDelete();
        }
    }

    onHandleClose = (e) => {
        e.preventDefault();
        if(this.props.onClose) {
            this.props.onClose(e);
        }
    }

    onHandleSave = (e) => {
        e.preventDefault();
        if(this.props.onSave) {
            this.props.onSave();
            if(this.props.onClose) {
                this.props.onClose(e);
            }

        }
    }

    render() {
        const { className, withControls } = this.props;
        return (
            <TooltipSettingWrap className={className}>
                <h2>Tooltip Settings</h2>
                {
                    withControls ?
                        <TooltipControls>
                            <ConfirmEditing href={'#'} onClick={this.onHandleSave}>✔</ConfirmEditing>
                            <CloseEditing href={'#'} onClick={this.onHandleClose}>✖</CloseEditing>
                        </TooltipControls>
                        :
                        null
                }

                <div>
                    <div>
                        <h3>Position</h3>
                        <SettingsList>
                            <SettingsItem><SettingsControl href="#" onClick={(e) => this.onHandleUpdateTooltip(e, {position: {top: 0, left: 0} })}>↖</SettingsControl></SettingsItem>
                            <SettingsItem><SettingsControl href="#" onClick={(e) => this.onHandleUpdateTooltip(e, {position: {top: 0, left: 130} })}>↑</SettingsControl></SettingsItem>
                            <SettingsItem><SettingsControl href="#" onClick={(e) => this.onHandleUpdateTooltip(e, {position: {top: 0, left: 260} })}>↗</SettingsControl></SettingsItem>
                            <SettingsItem><SettingsControl href="#" onClick={(e) => this.onHandleUpdateTooltip(e, {position: {top: 130, left: 0} })}>←</SettingsControl></SettingsItem>
                            <SettingsItem><SettingsControl href="#" onClick={(e) => this.onHandleUpdateTooltip(e, {position: {top: 130, left: 130} })}>⛶</SettingsControl></SettingsItem>
                            <SettingsItem><SettingsControl href="#" onClick={(e) => this.onHandleUpdateTooltip(e, {position: {top: 130, left: 260} })}>→</SettingsControl></SettingsItem>
                            <SettingsItem><SettingsControl href="#" onClick={(e) => this.onHandleUpdateTooltip(e, {position: {top: 260, left: 0} })}>↙</SettingsControl></SettingsItem>
                            <SettingsItem><SettingsControl href="#" onClick={(e) => this.onHandleUpdateTooltip(e, {position: {top: 260, left: 130} })}>↓</SettingsControl></SettingsItem>
                            <SettingsItem><SettingsControl href="#" onClick={(e) => this.onHandleUpdateTooltip(e, {position: {top: 260, left: 260} })}>↘</SettingsControl></SettingsItem>
                        </SettingsList>
                    </div>
                    <div>
                        <h3>Color</h3>
                        <SettingsList>
                            <SettingsColorItem><SettingsColorControl href="#"onClick={(e) => this.onHandleUpdateTooltip(e, {color: 'lime'})}></SettingsColorControl></SettingsColorItem>
                            <SettingsColorItem><SettingsColorControl href="#" onClick={(e) => this.onHandleUpdateTooltip(e, {color: 'tomato'})}></SettingsColorControl></SettingsColorItem>
                            <SettingsColorItem><SettingsColorControl href="#" onClick={(e) => this.onHandleUpdateTooltip(e, {color: 'red'})}></SettingsColorControl></SettingsColorItem>
                            <SettingsColorItem><SettingsColorControl href="#" onClick={(e) => this.onHandleUpdateTooltip(e, {color: 'orange'})}></SettingsColorControl></SettingsColorItem>
                            <SettingsColorItem><SettingsColorControl href="#" onClick={(e) => this.onHandleUpdateTooltip(e, {color: 'blue'})}></SettingsColorControl></SettingsColorItem>
                            <SettingsColorItem><SettingsColorControl href="#" onClick={(e) => this.onHandleUpdateTooltip(e, {color: 'purple'})}></SettingsColorControl></SettingsColorItem>
                            <SettingsColorItem><SettingsColorControl href="#" onClick={(e) => this.onHandleUpdateTooltip(e, {color: 'yellow'})}></SettingsColorControl></SettingsColorItem>
                            <SettingsColorItem><SettingsColorControl href="#" onClick={(e) => this.onHandleUpdateTooltip(e, {color: 'aqua'})}></SettingsColorControl></SettingsColorItem>
                            <SettingsColorItem><SettingsColorControl href="#" onClick={(e) => this.onHandleUpdateTooltip(e, {color: 'pink'})}></SettingsColorControl></SettingsColorItem>
                        </SettingsList>
                    </div>
                </div>
                <DeleteTooltipButton href={'#'} onClick={this.onHandleDelete}>Delete tooltip</DeleteTooltipButton>
            </TooltipSettingWrap>
        );
    }
}

export default TooltipSettings;
