import React, {Component} from 'react';
import {
    ModalHeader,
    ModalBody,
    ModalTitle,
    StyledInputFile,
    StyledForm,
    StyledSubmitBtn, StyledResetBtn, FormFooter, AddTooltipBtn, FormHeader, FormBody, StyledTooltipSettings
} from './style';
import {connect} from 'react-redux';
import {
    closeModalToAddImage,
    deleteTooltipUploadImage,
    setImageToUpload, setTooltipToUploadImage,
    updateTooltipUploadImage,
    uploadNewImage
} from '../../actions/image';
import Tooltip from '../tooltip';
import Modal from '../moodalWindow';

class AddImgModal extends Component {

    onHandleAddImage = (e) => {
        const file = e.target.files[0];
        if(file) {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => {
                this.props.setImageToUpload({ uploadImage: reader.result.replace(`data:${file.type};base64,`, ''), uploadFileType: file.type });
            }
        }
    }

    onHandleSubmit = (e) => {
        e.preventDefault();
        const { uploadNewImage, uploadImage, uploadFileType, uploadFileTooltip } = this.props;
        uploadNewImage({
            // Емуляция создания id картинки
            id: Math.floor(Math.random() * 150),
            // Сохраняем как base64 по скольку нет бека который отгружал бы картинку на сервер и отдавал ссылку на нее
            img: `data:${uploadFileType};base64,${uploadImage}`,
            // Tooltip на изображение
            tooltip: uploadFileTooltip,
        })
        this.props.setImageToUpload({ uploadImage: null, uploadFileType: null, uploadFileTooltip: null, });
    }

    handleCloseModal = (e) => {
        const { setImageToUpload, closeModalToAddImage } = this.props;
        setImageToUpload({ uploadImage: null, uploadFileType: null, uploadFileTooltip: null, });
        closeModalToAddImage();
    }

    onHandleAddTooltip = (e) => {
        e.preventDefault();
        this.props.setTooltipToUploadImage();
    }

    onHandleDeleteTooltip = () => {
        this.props.deleteTooltipUploadImage();
    }

    onHandleUpdateTooltip = (data) => {
        this.props.updateTooltipUploadImage({ ...data });
    }

    render() {
        const { uploadImage, uploadFileType, uploadFileTooltip } = this.props;
        return (
                <Modal onModalClose={this.handleCloseModal}>
                    <ModalHeader>
                        <ModalTitle>Add new image</ModalTitle>
                    </ModalHeader>
                    <ModalBody>
                        <StyledForm onSubmit={this.onHandleSubmit}>
                            <FormHeader>
                                {uploadImage ? <AddTooltipBtn type={'text'} onClick={this.onHandleAddTooltip} >Add tooltip</AddTooltipBtn> : null}
                                <StyledInputFile type="file" name={'image'} id={'image'} onChange={this.onHandleAddImage} file={uploadImage}/>
                                <label htmlFor="image">Select file</label>
                            </FormHeader>
                            {uploadFileTooltip ? <StyledTooltipSettings onDelete={this.onHandleDeleteTooltip} onUpdate={this.onHandleUpdateTooltip} withControls={false}/> : null}
                            <FormBody>
                                {
                                    uploadImage ? <img src={`data:${uploadFileType};base64,${uploadImage}`} alt="Upload"/> : null
                                }
                                {
                                    uploadFileTooltip ? <Tooltip
                                        key={uploadFileTooltip.id}
                                        id={uploadFileTooltip.id}
                                        tooltip={uploadFileTooltip}
                                        editable={true}
                                        onEdit={this.onHandleUpdateTooltip}
                                    /> : null
                                }
                            </FormBody>
                            <FormFooter>
                                <StyledResetBtn type={'reset'}>Cancel</StyledResetBtn>
                                {uploadImage ? <StyledSubmitBtn type={'submit'} >Upload</StyledSubmitBtn> : null}
                            </FormFooter>
                        </StyledForm>
                    </ModalBody>
                </Modal>
        );
    }
}

const mapDispatchToProps = {
    closeModalToAddImage,
    uploadNewImage,
    setImageToUpload,
    updateTooltipUploadImage,
    deleteTooltipUploadImage,
    setTooltipToUploadImage,
}

const mapStateToProps = ({imageReducer: { uploadImage, uploadFileType, uploadFileTooltip }}) => ({ uploadImage, uploadFileType, uploadFileTooltip });

export default connect(mapStateToProps, mapDispatchToProps)(AddImgModal);
