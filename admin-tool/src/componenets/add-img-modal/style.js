import style from 'styled-components';
import TooltipSettings from '../tooltip-settings';

export const ModalHeader = style.div`
    width: 100%;
    padding: 5px;
    text-align: center;
`;

export const ModalTitle = style.h1`
    font-size: 18px;
    color: #000;
    font-weight: bold;
    text-transform: uppercase;
`;

export const ModalBody = style.div`
    width: 100%;
    padding: 10px;
`;
export const FormBody = style.div`
        position: relative;
        width: 250px;
        height: 250px;
        font-size: 1.25em;
        font-weight: 700;
        color: #000;
        background-color: #fff;
        display: flex;
        justify-content: center;
        align-items: center;
        border: 1px solid #000;
        border-radius: 10px;
        overflow: hidden;
        padding: 5px;
        &::after {
            content: 'No image';
            display: ${props => props.file ? 'none' : 'flex'};
            justify-content: center;
            align-items: center;
            font-weight: bold;
            text-transform: uppercase;
        }
        & > img {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            object-fit: cover;         
        }
`;
export const FormHeader = style.div`
    display: flex;
    justify-content: center;
    align-items: flex-start;
    margin-bottom: 15px;
`;

export const CustomButton = style.button`
    width: 150px;
    height: 30px;
    border: none;
    overlay: none;
    border-radius: 5px;
    text-transform: uppercase;
    font-weight: bold;
    margin: 0 5px;
    color: #fff;
    &:hover {
        cursor: pointer;
    }
    &:disabled {
       background-color: #7d7d7dcc;
       color: #e2e2e2;
       cursor: not-allowed;
    }
`;

export const AddTooltipBtn = style(CustomButton)`
    background-color: #ff00009e;
    margin-bottom: 10px;
`;

export const StyledForm = style.form`
    display: flex;
    flex-flow: column;
    justify-content: flex-start;
    align-items: center;
`;

export const StyledInputFile = style.input`
    width: 0.1px;
    height: 0.1px;
    opacity: 0;
    overflow: hidden;
    position: absolute;
    z-index: -1;
    & + label {
        width: 150px;
        height: 30px;
        font-size: 14px;
        color: #fff;
        display: flex;
        justify-content: center;
        align-items: center;
        text-transform: uppercase;
        background-color: #ff00009e;
        margin: 0 5px;
        border-radius: 5px;
        &:hover {
            cursor: pointer;
        }
    }
`;

export const FormFooter = style(FormHeader)`
    margin-top: 15px;
`;

export const StyledSubmitBtn = style(CustomButton)`
    background-color: coral;  
`;

export const StyledResetBtn = style(CustomButton)`
    background-color: #ff00009e;
`;

export const StyledTooltipSettings = style(TooltipSettings)`
    margin-bottom: 10px;
`;
