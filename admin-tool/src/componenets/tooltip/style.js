import style from 'styled-components';

export const StyledWrap = style.div`
    position: absolute;
    display: flex;
    justify-content: flex-start;
    align-items: flex-start;
    transform: 
    ${props => props.tooltip.position.left === 130 && 'translateX(-50%)'} 
    ${props => props.tooltip.position.left === 260 && 'translateX(-100%)'}
    ${props => props.tooltip.position.top === 260 && 'translateY(-100%)'}
    ${props => props.tooltip.position.top === 130 && 'translateY(-50%)'};
    top: ${props => props.tooltip.position.top || 0}px;
    left: ${props => props.tooltip.position.left || 0}px;
    width:  ${props => props.tooltip.width || '120px'};
    height: ${props => props.tooltip.height || '30px'};
    max-width: 260px;
    max-height: 260px;
    word-wrap: break-word;
    z-index: 10;
    background-color: #636363e0;
    border-radius: 3px;
    overflow: hidden;
    resize: ${props => props.editable ? 'auto' : 'none'};
    & > p {
        padding: 5px;
        min-width: 100%;
        color: ${props => props.tooltip.color || '#fff'};
        font-size: 12px;
        margin: 0px;
        padding: 0px;
        outline: none;
        border: none;
        background: transparent;
    }
    & > textarea {
        position: absolute;
        top: 5px;
        left: 5px;
        padding: 5px;
        min-width: 100%;
        max-width: 100%;
        min-height: 100%;
        max-height: 100%;
        resize: none;
        overflow: hidden;
        color: ${props => props.tooltip.color || '#fff'};
        font-size: 12px;
        margin: 0px;
        outline: none;
        border: none;
        background: transparent;
    }
`;
