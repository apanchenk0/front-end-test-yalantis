import React, {Component} from 'react';
import {StyledWrap} from './style';
import * as ReactDOM from 'react-dom';

class Tooltip extends Component {

    state = {
        isEdit: false,
    }

    onHandleUpdateText = (e) => {
        e.preventDefault();
        if (this.props.onEdit) {
            this.props.onEdit({ text: e.target.value });
        }
    }

    onHandleResize = (e) => {
        const element = ReactDOM.findDOMNode(this);
        if (this.props.onEdit) {
            this.props.onEdit({ width: element.style.width, height: element.style.height });
        }
    }

    render() {
        const { tooltip, editable } = this.props;
        return (
            <StyledWrap onMouseUp={editable ? this.onHandleResize : null} onTouchEnd={editable ? this.onHandleResize : null} tooltip={tooltip} editable={editable} style={{width: tooltip.width || '120px', height: tooltip.height || '30px'}}>
                { editable ? <textarea value={tooltip.text} onChange={this.onHandleUpdateText} /> : <p>{tooltip.text}</p>}
            </StyledWrap>
        );
    }
}


export default Tooltip;
