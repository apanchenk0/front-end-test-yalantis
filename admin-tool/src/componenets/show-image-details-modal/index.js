import React, {Component} from 'react';
import Modal from '../moodalWindow';
import {connect} from 'react-redux';
import {Controls, DeleteButton, EditButton, EditingBox, InfoBox, StyledTooltipSettings} from './style';
import {
    deleteImage,
    deleteTooltipActiveImage,
    editTooltipActiveImage,
    hideImageDetails,
    saveEditionImage
} from '../../actions/image';
import ConfirmDeletingTooltip from './confirm-deleting-tooltip';
import Image from '../image';

class ShowDetailModal extends Component {

    state = {
        isDeleting: false,
        isTooltipEditing: false,
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        return prevProps.showedImage !== this.props.showedImage;
    }

    onCloseModal = () => {
        this.props.hideImageDetails();
    }

    onHandleConfirmDelete = (e) => {
        e.preventDefault();
        this.props.deleteImage(this.props.showedImage.id);
    }

    onHandleDeleteStatus = (e) => {
        e.preventDefault();
        this.setState((state) => ({
            isDeleting: !state.isDeleting,
            isTooltipEditing: false,
        }));
    }

    onHandleTooltipSettings = (e) => {
        e.preventDefault();
        this.setState((state) => ({
            isTooltipEditing: !state.isTooltipEditing,
            isDeleting: false,
        }));
    }

    onHandleUpdateTooltip = (data) => {
        this.props.editTooltipActiveImage(data);
    }

    onHandleSaveUpdatingTooltip = () => {
        this.props.saveEditionImage(this.props.showedImage);
    }

    onHandleDeleteTooltip = () => {
        this.props.deleteTooltipActiveImage();
    }

    render() {
        const { showedImage } = this.props;
        const { isTooltipEditing, isDeleting } = this.state;
        return (
            <Modal onModalClose={this.onCloseModal}>
                <InfoBox isTooltipEditing={isTooltipEditing}>
                    <h2>Image number {showedImage.id}</h2>
                    <Image data={showedImage} editable={isTooltipEditing} onEdit={isTooltipEditing && this.onHandleUpdateTooltip} tooltipAlways={isTooltipEditing} noAnimate={true}/>
                </InfoBox>
                <EditingBox>
                    <Controls>
                        <DeleteButton onClick={this.onHandleDeleteStatus}>Delete</DeleteButton>
                        {isDeleting ? <ConfirmDeletingTooltip onConfirm={this.onHandleConfirmDelete} onCancel={this.onHandleDeleteStatus}/> : null}
                        <EditButton onClick={this.onHandleTooltipSettings}>Edit tooltip</EditButton>
                        {isTooltipEditing ? <StyledTooltipSettings onUpdate={this.onHandleUpdateTooltip} withControls={true} onClose={this.onHandleTooltipSettings} onSave={this.onHandleSaveUpdatingTooltip} onDelete={this.onHandleDeleteTooltip}/> : null}
                    </Controls>
                </EditingBox>
            </Modal>
        );
    }
}

const mapStateToProps = ({imageReducer: { showedImage } }) => ({showedImage});

const mapDispatchToProps = {
    hideImageDetails,
    deleteImage,
    editTooltipActiveImage,
    saveEditionImage,
    deleteTooltipActiveImage,
}

export default connect(mapStateToProps, mapDispatchToProps)(ShowDetailModal);
