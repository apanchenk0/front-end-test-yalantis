import React from 'react';
import {ConfirmDeletingTooltipWrap, Control} from './style';

const ConfirmDeletingTooltip = ({ onConfirm, onCancel }) => {
    return (
        <ConfirmDeletingTooltipWrap>
            <h2>Confirm deletion</h2>
            <div>
                <Control onClick={onConfirm}>Confirm</Control>
                <Control onClick={onCancel}>Cancel</Control>
            </div>
        </ConfirmDeletingTooltipWrap>
    )
}

export default ConfirmDeletingTooltip;
