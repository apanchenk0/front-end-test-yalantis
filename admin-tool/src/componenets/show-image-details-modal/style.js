import style from 'styled-components';
import TooltipSettings from '../tooltip-settings';

export const InfoBox = style.div`
    flex: 0 0 50%;
    max-width: 50%;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-flow: column;
    padding: 10px;
    & > h2 {
        font-size: 18px;
        font-weight: bold;
        color: coral;
        text-transform: uppercase;
    }
    & > div {
        width: 260px;
        height: 260px;
        position: relative;
        &:hover {
            cursor: ${props => props.isTooltipEditing ? 'default' : 'pointer'};
        }
        & > img {
            width: 260px;
            height: 260px;
            object-fit: cover;
        }
    }
`;

export const EditingBox = style.div`
    flex: 0 0 505;
    max-width: 50%;
    display: flex;
    justify-content: flex-start;
    align-items: center;
    flex-flow: column;
`;

export const Controls = style.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    position: relative;
`;

const Button = style.button`
    width: 130px;
    height: 30px;
    font-size: 14px;
    color: #fff;
    font-weight: bold;
    text-transform: uppercase;
    outline: none;
    border: none;
    border-radius: 5px;
    margin 5px;
    position: relative;
    &:hover {
        cursor: pointer;
    }
`;

export const DeleteButton = style(Button)`
    background-color: #ff00009e;
`;

export const EditButton = style(Button)`
    background-color: #66bb67;
`;

export const ConfirmDeletingTooltipWrap = style.div`
    position: absolute;
    left: 25%;
    top: 40px;
    transform: translateX(-50%);
    padding: 10px;
    background-color: #797979;
    border-radius: 10px;
    & > h2 {
        font-size: 12px;
        color: #fff;
        font-weight: bold;
        text-align: center;
        text-transform: uppercase;
    }
    & > div {
        display: flex;
        justify-content: space-between;
        align-items: center;
    }
    &::before {
        content: '';
        border: 5px solid transparent;
        border-bottom: 10px solid #797979;
        position: absolute;
        top: -10px;
        left: 50%;
        transform: translateX(-50%);
    }
`;

export const Control = style.a`
    text-decoration: none;
    width: 80px;
    height: 20px;
    color: #fff;
    border-radius: 3px;
    font-size: 10px;
    text-transform: uppercase;
    font-weight: bold;
    outline: none;
    box-shadow: none;
    border: none;
    display: flex;
    justify-content: center;
    align-items: center;
    margin: 0 5px;
    &:hover {
        cursor: pointer;
    }
    &:first-child {
        background-color: #64f764;
    }
    &:last-child {
        background-color: #ff00009e;
    }
`;

export const StyledTooltipSettings = style(TooltipSettings)`
    position: absolute;
    top: 100%;
    right: 0px;
`;
