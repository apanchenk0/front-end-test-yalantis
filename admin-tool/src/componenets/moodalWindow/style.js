import style, {keyframes} from 'styled-components';

const fadeIn = keyframes`
    from {
        opacity: 0;
    }
    to {
        opacity : 1;
    }
`;

const show = keyframes`
    from {
        top: -100%;
        opacity: 0;
    }
    50% {
        opacity: 1;
    }
    to {
        top: 30px;
    }
`;

export const Overlay = style.div`
    position: fixed;
    top: 0;
    left: 0;
    z-index: 1000;
    width: 100%;
    height: 100vh;
    background-color: #1e1e1ee8;
    animation: ${fadeIn} 0.5s ease-in;
`;

export const Window = style.div`
    display: flex;
    justify-content: center;
    align-items: center;
    flex-wrap: wrap;
    width: 50%;
    position: absolute;
    top: 30px;
    left: 50%;
    transform: translateX(-50%);
    height: auto;
    animation: ${show} 0.75s ease-in;
    border-radius: 15px;
    background-color: #fff;
    @media screen and (max-width: 720px) {
        width: 75%;
    }
    @media screen and (max-width: 560px) {
        width: 90%;
    }
`;
