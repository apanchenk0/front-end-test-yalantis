import React, {Component} from 'react';
import {Overlay, Window} from './style';
import * as ReactDOM from 'react-dom';

class Modal extends Component {

    componentDidMount() {
        document.addEventListener('mousedown', this.handleCloseModal, false);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleCloseModal, false);
    }

    handleCloseModal = (e) => {
        const domNode = ReactDOM.findDOMNode(this);
        if(domNode === e.target || e.target.type === 'reset') {
            return this.props.onModalClose ? this.props.onModalClose() : true;
        }
    }

    render() {
        return (
            <Overlay >
                <Window>
                    {this.props.children}
                </Window>
            </Overlay>
        );
    }
}

export default Modal;
