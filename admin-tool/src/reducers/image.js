import {
    SET_IMAGE_DATA,
    OPEN_ADD_IMAGE_MODAL,
    CLOSE_ADD_IMAGE_MODAL,
    SET_IMAGE_TO_UPLOAD,
    UPDATE_TOOLTIP_UPLOAD_IMAGE,
    SHOW_IMAGE_DETAILS, HIDE_IMAGE_DETAILS, DELETE_TOOLTIP_UPLOAD_IMAGE,
    ADD_TOOLTIP_TO_UPLOAD_IMAGE, EDIT_TOOLTIP_ACTIVE_IMAGE, IS_IMAGES_LOADING, DELETE_TOOLTIP_ACTIVE_IMAGE,
} from '../types/image';

const initialState = {
    images: [],
    isImagesLoading: false,
    addImage: null,
    isModalOpen: false,
    uploadImage: null,
    uploadFileType: null,
    uploadFileTooltip: null,
    showedImage: null,
    isShowedImageEdit: false,
}

export function imageReducer(state = initialState, action) {
    switch (action.type) {
        case SET_IMAGE_DATA:
            return { ...state, images: action.payload };
        case IS_IMAGES_LOADING:
            return { ...state, isImagesLoading: action.payload };
        case OPEN_ADD_IMAGE_MODAL:
            return { ...state, isModalOpen: true };
        case CLOSE_ADD_IMAGE_MODAL:
            return { ...state, isModalOpen: false };
        case SET_IMAGE_TO_UPLOAD:
            return { ...state, ...action.payload };
        case ADD_TOOLTIP_TO_UPLOAD_IMAGE:
            return { ...state, uploadFileTooltip: { id: 0, color: '#fff', position: {top: 0, left: 0}, text: '', } };
        case UPDATE_TOOLTIP_UPLOAD_IMAGE:
            return { ...state, uploadFileTooltip: { ...state.uploadFileTooltip, ...action.payload} };
        case DELETE_TOOLTIP_UPLOAD_IMAGE:
            return { ...state, uploadFileTooltip: null };
        case SHOW_IMAGE_DETAILS:
            const image = state.images.filter((img) => img.id === action.payload);
            return { ...state, showedImage: image[0] };
        case HIDE_IMAGE_DETAILS:
            return { ...state, showedImage: null};
        case EDIT_TOOLTIP_ACTIVE_IMAGE:
            const editedShowedImage = { ...state.showedImage, tooltip: { ...state.showedImage.tooltip, ...action.payload }};
            return { ...state, showedImage: editedShowedImage };
        case DELETE_TOOLTIP_ACTIVE_IMAGE:
            const newShowedImage = {...state.showedImage, tooltip: null };
            return { ...state, showedImage: newShowedImage };
        default:
            return state;
    }
}
