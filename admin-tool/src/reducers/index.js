import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import { imageReducer } from './image';

const createMainReducer = (history) => (
    combineReducers({
        router: connectRouter(history),
        imageReducer,
    })
);

export default createMainReducer;
