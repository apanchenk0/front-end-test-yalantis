import {
    CLOSE_ADD_IMAGE_MODAL,
    LOAD_ALL_IMAGES,
    OPEN_ADD_IMAGE_MODAL,
    SET_IMAGE_TO_UPLOAD,
    UPDATE_TOOLTIP_UPLOAD_IMAGE,
    UPLOAD_IMAGE,
    SHOW_IMAGE_DETAILS,
    HIDE_IMAGE_DETAILS,
    DELETE_IMAGE,
    DELETE_TOOLTIP_UPLOAD_IMAGE,
    ADD_TOOLTIP_TO_UPLOAD_IMAGE,
    EDIT_TOOLTIP_ACTIVE_IMAGE,
    SAVE_EDITION_IMAGE,
    DELETE_TOOLTIP_ACTIVE_IMAGE,
} from '../types/image';

export const loadAllImages = (data) => ({
    type: LOAD_ALL_IMAGES,
    payload: data,
})

export const openModalToAddImage = () => ({
    type: OPEN_ADD_IMAGE_MODAL,
})

export const closeModalToAddImage = () => ({
    type: CLOSE_ADD_IMAGE_MODAL,
})

export const uploadNewImage = (data) => ({
    type: UPLOAD_IMAGE,
    payload: data,
})

export const setImageToUpload = (data) => ({
    type: SET_IMAGE_TO_UPLOAD,
    payload: data,
})

export const setTooltipToUploadImage = () => ({
    type: ADD_TOOLTIP_TO_UPLOAD_IMAGE,
})

export const updateTooltipUploadImage = (data) => ({
    type: UPDATE_TOOLTIP_UPLOAD_IMAGE,
    payload: data,
})

export const deleteTooltipUploadImage = () => ({
    type: DELETE_TOOLTIP_UPLOAD_IMAGE,
})

export const showImageDetails = (data) => ({
    type: SHOW_IMAGE_DETAILS,
    payload: data,
})

export const hideImageDetails = () => ({
    type: HIDE_IMAGE_DETAILS,
})

export const deleteImage = (id) => ({
    type: DELETE_IMAGE,
    payload: id,
})

export const editTooltipActiveImage = (data) => ({
    type: EDIT_TOOLTIP_ACTIVE_IMAGE,
    payload: data,
});

export const deleteTooltipActiveImage = () => ({
    type: DELETE_TOOLTIP_ACTIVE_IMAGE,
})

export const saveEditionImage = (data) => ({
    type: SAVE_EDITION_IMAGE,
    payload: data,
})
