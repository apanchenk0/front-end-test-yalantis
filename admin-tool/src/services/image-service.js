let instance = null;

export default class ImageService {
    constructor() {
        instance = instance || this;
        return instance;
    }

    // Получаем кастомный шаблон с сервера и кладеам его в localStorage. localStorage будет служить базой данных
    loadAll = async () => {
        if(localStorage.getItem('data')){
            const response = JSON.parse(localStorage.getItem('data'));
            return response;
        } else {
            let response = await fetch('http://www.json-generator.com/api/json/get/bOZrUyZSrm?indent=2').then(res => res);
            response = await response.json().then(response => response);
            localStorage.setItem('data', JSON.stringify(response));
            return response;
        }
    }

    // Загрузка нового изображения
    uploadOne = (image) => {
        const data = JSON.parse(localStorage.getItem('data') || '[]');
        data.push(image);
        localStorage.setItem('data', JSON.stringify(data));
    }

    // Удаление изображения
    deleteOne = (id) => {
        const data = JSON.parse(localStorage.getItem('data') || '[]');
        const newData = data.filter(element => element.id !== id);
        localStorage.setItem('data', JSON.stringify(newData));
    }

    // Обновление изображения
    updateOne = (image) => {
        const data = JSON.parse(localStorage.getItem('data'));
        const oldElement = data.filter(elem => elem.id === image.id)[0];
        const oldElementIndex = data.indexOf(oldElement);
        data.splice(oldElementIndex, 1, image);
        localStorage.setItem('data', JSON.stringify(data));
    }
}
